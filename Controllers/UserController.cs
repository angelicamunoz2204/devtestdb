using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using DevTestDB.Repositories;
using System.Threading.Tasks;

namespace DevTest.Controllers
{
    [ApiController]
    [Route("api/user")]
    
    public class UserControllers : ControllerBase
    {
       private IUserRepository _userRepository;
       public UserControllers(IUserRepository userRepository)
       {
           _userRepository = userRepository;
       }

        [HttpGet]
        public async Task<IActionResult> GetAllUsersAsync()
        {
           return Ok(await _userRepository.GetAllUsers());
        }
        [HttpPost]
        public async Task<IActionResult> NewUserAsync(User user)
        {
            User userN = await _userRepository.NewUser(user);
            return Ok($"User with ID {userN.UserId} added successfully!");
        }
        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUserByIdAsync(Guid userId)
        {
            User userG = await _userRepository.GetUserById(userId);
           return Ok($"The user with ID {userId} is {userG.UserName}");
        }
        [HttpPut]
        public async Task<IActionResult> UpdateUserAsync(User user)
        {
            User userU = await _userRepository.UpdateUser(user);
            return Ok($"User with ID {userU.UserId} has the name {userU.UserName} now!" );
        }
        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUserAsync(Guid userId)
        {
            await _userRepository.DeleteUser(userId);
            return Ok($"The user with ID {userId} has been deleted" );
        }

    }
}