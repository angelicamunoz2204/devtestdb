using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using DevTestDB.Components;
using DevTestDB.Repositories;
using System.Threading.Tasks;

namespace DevTest.Controllers
{
    [ApiController]
    [Route("api/bet")]
    
    public class BetControllers : ControllerBase
    {
        private IBetRepository _betRepository;
        public BetControllers(IBetRepository betRepository)
        {
           _betRepository = betRepository;
        }

        [HttpPost]
        public async Task<IActionResult> NewBetAsync(Bet bet)
        {
           await _betRepository.NewBet(bet);
           return Ok($"Bet with ID {bet.BetId} added successfully!");
        }
        [HttpGet]
        public async Task<IActionResult> GetAllBetsAsync()
        {
           return Ok(await _betRepository.GetAllBets());
        }
        [HttpGet("Bet/User/{userId}")]
        public async Task<IActionResult> GetBetAmountByIdAsync(Guid userId) 
        {
           Bet betG = await _betRepository.GetBetAmountById(userId);
           return Ok($"The bet with USD {betG.BetAmount} has done by the user with ID {userId}");
        }
        [HttpPut]
        public async Task<IActionResult> UpdateBetAsync(Bet bet)
        {
            Bet betU = await _betRepository.UpdateBet(bet);
            return Ok($"Bet with ID {betU.BetId} has the amount {betU.BetAmount} now!" );
        }
        [HttpDelete("{betId}")]
        public async Task<IActionResult> DeleteBetAsync(Guid betId)
        {
            await _betRepository.DeleteBet(betId);
            return Ok($"The bet with ID {betId} has been deleted" );
        }
    }
}