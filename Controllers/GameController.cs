using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using DevTestDB.Repositories;
using System.Threading.Tasks;

namespace DevTest.Controllers
{
    [ApiController]
    [Route("api/game")]
    
    public class GameControllers : ControllerBase
    {
        private IGameRepository _gameRepository;
        public GameControllers(IGameRepository gameRepository)
        {
           _gameRepository = gameRepository;
        }

        [HttpPost]
        public async Task<IActionResult> NewGameAsync(Game game)
        {
            await _gameRepository.NewGame(game);
           return Ok($"Game with ID {game.GameId} added successfully!");
        }
        [HttpGet]
        public async Task<IActionResult> GetAllGamesAsync()
        {
           return Ok(await _gameRepository.GetAllGames());
        }
        [HttpGet("{gameId}")]
        public async Task<IActionResult> GetGameTeamsByIdAsync(Guid gameId)
        {
           Game gameT = await _gameRepository.GetGameTeamsById(gameId);
           return Ok($"The teams of the game with ID {gameId} are {gameT.GameHostTeam} and {gameT.GameGuestTeam}");
        }
        [HttpPut]
        public async Task<IActionResult> UpdateGameAsync(Game game)
        {
            Game gameU = await _gameRepository.UpdateGame(game);
            return Ok($"Game with ID {gameU.GameId} has the date {gameU.GameDate} now!" );
        }
        [HttpDelete("{gameId}")]
        public async Task<IActionResult> DeleteGameAsync(Guid gameId)
        {
            await _gameRepository.DeleteGame(gameId);
            return Ok($"The game with ID {gameId} has been deleted" );
        }
    }
}