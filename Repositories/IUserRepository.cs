using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevTest;

namespace DevTestDB.Repositories
{
    public interface IUserRepository
    {
        Task<User> NewUser(User user);
        Task<IEnumerable<User>> GetAllUsers();
        Task<User> GetUserById(Guid userId);
        Task<User> UpdateUser(User user);
        Task DeleteUser(Guid userId);
    }
}