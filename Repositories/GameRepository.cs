using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevTest;
using DevTestDB.Components;
using Microsoft.EntityFrameworkCore;

namespace DevTestDB.Repositories
{
    public class GameRepository: IGameRepository
    {
        private readonly IBetsAppContext _gameContext;
        public GameRepository(IBetsAppContext gameContext)
        {
            _gameContext = gameContext;
        }
        public async Task<Game> NewGame(Game game)
        {
            _gameContext.Games.Add(game);
            await _gameContext.SaveChangesAsync(); 
            return game;
        }
        public async Task<IEnumerable<Game>> GetAllGames()
        {
           return await _gameContext.Games.ToListAsync();
        }
        public async Task<Game> GetGameTeamsById(Guid gameId) 
        {
            Game gameT = await _gameContext.Games.FindAsync(gameId); 
            if(gameT == null)
            {
                throw new NullReferenceException();
            }
            await _gameContext.SaveChangesAsync();
            return gameT;

        }
        public async Task<Game> UpdateGame(Game game)
        {
            Game gameU = await _gameContext.Games.FindAsync(game.GameId);
            if (gameU == null)
            {
                throw new NullReferenceException();
            }
            gameU.GameDate = game.GameDate;
            await _gameContext.SaveChangesAsync();
            return gameU;
        }
        public async Task DeleteGame(Guid gameId)
        {
            Game gameD = await _gameContext.Games.FindAsync(gameId);
            if(gameD == null)
            {
                throw new NullReferenceException();
            }
            _gameContext.Games.Remove(gameD);
            await _gameContext.SaveChangesAsync();
        }
    }
}