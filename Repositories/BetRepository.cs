using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevTest;
using DevTestDB.Components;
using Microsoft.EntityFrameworkCore;

namespace DevTestDB.Repositories
{
    public class BetRepository : IBetRepository
    {
        private readonly IBetsAppContext _betContext;
        public BetRepository(IBetsAppContext betContext)
        {
            _betContext = betContext;
        }
        public async Task<Bet> NewBet(Bet bet)
        {
            _betContext.Bets.Add(bet);
            await _betContext.SaveChangesAsync(); 
            return bet;
        }
        public async Task<IEnumerable<Bet>> GetAllBets()
        {
           return await _betContext.Bets.ToListAsync();
        }
        public async Task<Bet> GetBetAmountById(Guid userId) 
        {
            Bet betA = await _betContext.Bets.FindAsync(userId); 
            if (betA == null)
            {
                throw new NullReferenceException();
            }
            await _betContext.SaveChangesAsync();
            return betA;
        }
        public async Task<Bet> UpdateBet(Bet bet)
        {
            Bet betU = await _betContext.Bets.FindAsync(bet.BetId);
            if (betU == null)
            {
                throw new NullReferenceException();
            }
            betU.BetAmount = bet.BetAmount;
            await _betContext.SaveChangesAsync();
            return betU;
        }
        public async Task DeleteBet(Guid betId)
        {
            Bet betD = await _betContext.Bets.FindAsync(betId);
            if(betD == null)
            {
                throw new NullReferenceException();
            }
            _betContext.Bets.Remove(betD);
            await _betContext.SaveChangesAsync();
        }
        
    }
}