using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevTest;
using DevTestDB.Components;
using Microsoft.EntityFrameworkCore;

namespace DevTestDB.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IBetsAppContext _userContext;
        public UserRepository(IBetsAppContext userContext)
        {
            _userContext = userContext;
        }
        public async Task<User> NewUser(User user)
        {
            _userContext.Users.Add(user);
            await _userContext.SaveChangesAsync(); 
            return user;
        }
        public async Task<IEnumerable<User>> GetAllUsers()
        {
           return await _userContext.Users.ToListAsync();
        }
        public async Task<User> GetUserById(Guid userId) 
        {
            User userG = await _userContext.Users.FindAsync(userId); 
            if (userG == null)
            {
                throw new NullReferenceException();
            }
            await _userContext.SaveChangesAsync();
            return userG;

        }
        public async Task<User> UpdateUser(User user)
        {
            var userU = await _userContext.Users.FindAsync(user.UserId);
            if (userU == null)
            {
                throw new NullReferenceException();
            }
            userU.UserName = user.UserName;
            await _userContext.SaveChangesAsync();
            return userU;
        }
        public async Task DeleteUser(Guid userId)
        {
            var userD = await _userContext.Users.FindAsync(userId);
            if(userD == null)
            {
                throw new NullReferenceException();
            }
            _userContext.Users.Remove(userD);
            await _userContext.SaveChangesAsync();
        }
    }
}