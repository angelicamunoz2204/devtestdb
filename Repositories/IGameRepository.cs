using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevTest;

namespace DevTestDB.Repositories
{
    public interface IGameRepository
    {
        Task<Game> NewGame(Game game);
        Task<IEnumerable<Game>> GetAllGames();
        Task<Game> GetGameTeamsById(Guid gameId);
        Task<Game> UpdateGame(Game game);
        Task DeleteGame(Guid gameId);
    }
}