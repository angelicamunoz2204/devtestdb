using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevTest;

namespace DevTestDB.Repositories
{
    public interface IBetRepository
    {
        Task<Bet> NewBet(Bet bet);
        Task<IEnumerable<Bet>> GetAllBets();
        Task<Bet> GetBetAmountById(Guid userId);
        Task<Bet> UpdateBet(Bet bet);
        Task DeleteBet(Guid betId);
    }
}