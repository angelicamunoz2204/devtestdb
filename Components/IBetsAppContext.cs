using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DevTest;
using Microsoft.EntityFrameworkCore;

namespace DevTestDB.Components
{
    public interface IBetsAppContext
    {
        DbSet<Bet> Bets { get; init; }
        DbSet <User> Users { get; init; }
        DbSet <Game> Games { get; init; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}