using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace DevTest
{
    public class User
    {
        [Key]
        public Guid UserId { get; set; }
        [Required]
        public string UserName { get; set; }
        public ICollection<Bet> Bets { get; set; }
    }
}
  