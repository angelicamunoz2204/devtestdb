using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace DevTest
{
    public class Game
    {
        [Key]
        public Guid GameId { get; set; }
        [Required]
        public string GameHostTeam { get; set; }
        [Required]
        public string GameGuestTeam { get; set; }
        [Required]
        public int GameLocalTeamStore { get; set; }
        [Required]
        public int GameGuestTeamStore { get; set; }
        [Required]
        public DateTime GameDate { get; set; }
        [Required]
        public ICollection<Bet> Bets { get; set; }
    }
}