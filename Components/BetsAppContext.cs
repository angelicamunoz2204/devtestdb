using DevTestDB.Components;
using Microsoft.EntityFrameworkCore; 
namespace DevTest
{
    public class BetsAppContext: DbContext, IBetsAppContext
    {
        public BetsAppContext(DbContextOptions<BetsAppContext> options) : base(options)
        {
            
        }
        public DbSet <Bet> Bets { get; init; }
        public DbSet <User> Users { get; init; }
        public DbSet <Game> Games { get; init; }
    }
}
