using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DevTest
{
    public class Bet
    {
        [Key]
        public Guid BetId { get; set; }
        [ForeignKey("Game")]
        public Guid GameId { get; set; }
        public Game Game { get; set; }
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public User User { get; set; }
        [Required]
        public int BetAmount { get; set; }
        [Required]
        public int GameLocalTeamStore { get; set; }
        [Required]
        public int GameGuestTeamStore { get; set; }
        [Required]
        public bool BetCompleted { get; set; }

    }
}